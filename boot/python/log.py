import sys
from time import strftime
from caramelos.fs.funcs import open, exists, direxists, mkdir, delete

class Logger():
    def __init__(self, out = None):
        if not out:
            if exists('/log/sys.log'):
                delete('/log/sys.log')
            out = open('/log/sys.log', 'w')
        self.out = out

    def log(self, msg, level):
        level = level.upper()
        self.out.write('Log - {0} @ {1}: {2}'.format(level, strftime('%c'), msg))

    def warn(self, msg):
        self.log(msg, 'warn')

    def info(self, msg):
        self.log(msg, 'info')

    def error(self, msg):
        self.log(msg, 'error')

    def critical(self, msg):
        self.log(msg, 'critical')

    def halt(self):
        self.log('SYSTEM ERROR - KERNEL HALT', 'halt')

syslog = Logger()