'''Instructions for the parser
Should take an instance of Parser
'''

# Imports
from . import utils

exclude = ['utils']

def add(parser):
    args = utils.getstack(parser, numargs = 2)
    if utils.checkargs(args, type = int):
        parser.push(args[0] + args[1])

    else:
        parser.error('[ADD] Stack had non-integer values')

def sub(parser):
    args = utils.getstack(parser, numargs = 2)
    if utils.checkargs(args, type = int):
        parser.push(args[0] - args[1])

    else:
        parser.error('[SUB] Stack had non-integer values')

def mul(parser):
    args = utils.getstack(parser, numargs = 2)
    if utils.checkargs(args, type = int):
        parser.push(args[0] * args[1])

    else:
        parser.error('[MUL] Stack had non-integer values')

def div(parser):
    args = utils.getstack(parser, numargs = 2)
    if utils.checkargs(args, type = int):
        if utils.checkargs(args, nonzero = True):
            parser.push(args[0])

        else:
            parser.error('[DIV] Stack had values that weren\'t nonzero')

    else:
        parser.error('[DIV] Stack had non-integer values')

def dup(parser):
    arg = utils.getstack(parser)
    parser.push(arg)
    parser.push(arg)

def exit(parser):
    parser.error('[EXIT] Bye!')

def print(parser):
    arg = utils.getstack(parser)
    if utils.checkargs([arg], type = str):
        print(arg, end = '')

    else:
        parser.error('[PRINT] Stack had non-str value')

def println(parser):
    arg = utils.getstack(parser)
    if utils.checkargs([arg], type = str):
        print(arg)

    else:
        parser.error('[PRINTLN] Stack had non-str value')

def jmp(parser):
    arg = utils.getstack(parser)
    if utils.checkargs([arg], type = str):
        if arg in parser._labels:
            parser.ret_stack.push(parser.ip)

        else:
            parser.error('[JMP] No label {0}'.format(arg))

    if utils.checkargs([arg], type = int):
        if arg < parser.length and arg > 0:
            parser.ip = arg

        else:
            parser.error('[JMP] Address {0} above max {1} or less than 0'.format(arg, parser.length - 1))

    else:
        parser.error('[JMP] Invalid address type')

def read(parser):
    parser.push(input())

def eq(parser):
    args = utils.getstack(parser, numargs=2)
    return args[0] == args[1]