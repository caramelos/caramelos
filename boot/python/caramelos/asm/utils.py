'''Utilities for the instructions'''

# Imports

# Error import
from .errors import *

class ArgumentChecker():
    def __init__(self, arguments, *args, **kwargs):
        self.args = arguments
        self.kwargs = kwargs

    def check_type(self, destype, args):
        for argid in range(len(args)):
            arg = args[argid]
            if isinstance(arg, destype): continue
            return False
        return True

    def check_nonzero(self, _, args):
        for i in range(len(args)):
            arg = args[i]
            if arg != 0: return True
        return False

    def check(self):
        '''Gets all functions starting with the string check and runs them
        with the keyword arguments and arguments provided'''
        tests = {k: getattr(self, k) for k in dir(self) if k.startswith('check_') and k[6:] in self.kwargs}
        results = [tests[i](self.kwargs[i[6:]], self.args) for i in tests]
        return all(results)

def checkargs(args, **kwargs):
    return ArgumentChecker(args, **kwargs).check()

def getstack(parser, numargs = 1):
    if len(parser.stack) >= numargs:
        return [parser.pop() for i in range(numargs)]

    else:
        raise ASMStackError('Only {0} values on stack, {1} required'.format(len(parser.stack), numargs))