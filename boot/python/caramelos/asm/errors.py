'''Errors for the parser'''

# Any error
class ASMError(Exception):
    pass

# Syntax error
class ASMSyntaxError(ASMError):
    pass

# Bad indent
class ASMIndentError(ASMSyntaxError):
    pass

# Stack error
class ASMStackError(ASMError):
    pass