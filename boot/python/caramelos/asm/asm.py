'''Assembly-style parser'''

# Imports
import re
from collections import deque

# Instruction import and list conversion
from . import instructions as instrmod
re_private = re.compile(r'__.*__')
instr = {k.lower(): getattr(instrmod, k) for k in dir(instrmod) if not re_private.match(k) or k in instrmod.exclude \
         or k == 'exclude' and callable(getattr(instrmod, k))}
del instrmod

# Error import
from .errors import *

# Define RegExs
re_label = re.compile(r'^(.+):$')
re_ident = re.compile(r'^    .*')
re_removespaces = re.compile(r'\s*(.*)\s*')
re_int = re.compile(r'^\d+$')

# Custom deque with push and top methods
class Stack(deque):
    push = deque.append

    def top(self):
        return self[-1]

# Parser
class Parser():
    def __init__(self, code):
        self.code = code
        self.ip = 0
        self.stack = Stack()
        self.ret_stack = Stack()
        self.err_stack = Stack()
        self._label = None
        self._labels = []
        self.length = len(''.join(self.code).replace('\n', ' ').split(' '))

        self.push = self.stack.push
        self.pop = self.stack.pop
        self.top = self.stack.top

        self.error = self.err_stack.push

    def parse(self, line):
        if line.replace(' ', '') == '': return

        if self._label:
            identm = re_ident.match(line)
            if not identm:
                self._label = None

        if not self._label:
            identm = re_ident.match(line)
            if identm:
                raise ASMIndentError('Can\'t use indent out of label')

        labelm = re_label.match(line)
        if labelm:
            if self._label:
                raise ASMSyntaxError('Can\'t have sublabels')
            self._label = labelm.group(1)
            self._labels[self._label] = []
            return

        nospacem = re_removespaces.match(line)
        if nospacem:
            name = nospacem.group(1)
            if name in instr:
                instr[name](self)
                if len(self.err_stack) > 0:
                    raise ASMError('Line errored - line content = {0} - top var on err_stack = {1}'.format(line, self.err_stack.top()))

            else:
                intm = re_int.match(name)
                if intm:
                    self.push(int(name))

                else:
                    self.push(name)

    def run(self):
        code = ''.join(self.code).replace('\n', ' ').split(' ')
        while self.ip < len(code):
            self.parse(code[self.ip])
            self.ip += 1