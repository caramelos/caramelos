class User():
    def __init__(self, name, privs):
        self.name = name
        self.privs = privs

class LoginMngr():
    def __init__(self):
        self.users = {}

    def adduser(self, name, privs):
        self.users.update({name: User(name, privs)})

    def deluser(self, name):
        self.users.pop(name)

    def setprivs(self, name, privs):
        self.users[name] = privs

lm = LoginMngr()