class LMError(Exception):
    pass

class PrivilegeError(LMError):
    pass

class PrivilegeExistsError(PrivilegeError):
    pass

class PrivilegeNotFoundError(PrivilegeError):
    pass

class UserExistsError(LMError):
    pass

class UserNotFoundError(LMError):
    pass

class LM():
    def __init__(self):
        self.privs = ['kernel', 'root']
        self.users = {'kernel': ['kernel', 'root'], 'root': ['root']}
        self._current = 'kernel'

    @property
    def current(self):
        return self._current

    @current.setter
    def login(self, name):
        if name == 'kernel':
            raise LMError('You can\'t login as the kernel user')

        if name in self.users:
            self._current = name

        else:
            raise UserNotFoundError('User {0} doesn\'t exist'.format(name))

    def add_priv(self, name):
        if name not in self.privs:
            self.privs.append(name)

        else:
            raise PrivilegeExistsError('Privilege {0} already exists'.format(name))

    def delete_priv(self, name):
        if name in self.privs:
            self.privs.pop(name)

        else:
            raise PrivilegeNotFoundError('Privilege {0} doesn\'t exist'.format(name))

    def rename_priv(self, old, new):
        self.delete_priv(old)
        self.add_priv(new)

    def add_user(self, name):
        if name not in self.users:
            self.users.update({name: []})

        else:
            raise UserExistsError('User {0} already exists'.format(name))

    def delete_user(self, name):
        if name in self.users:
            del self.users[name]

        else:
            raise UserNotFoundError('User {0} doesn\'t exist'.format(name))

    def give_name(self, name, priv):
        if name in self.users:
            if priv in self.privs:
                self.users[name].append(priv)

            else:
                raise PrivilegeNotFoundError('Privilege {0} doesn\'t exist'.format(name))

        else:
            raise UserNotFoundError('User {0} doesn\'t exist'.format(name))

    def take_name(self, name, priv):
        if name in self.users:
            if priv in self.users[name]:
                self.users[name].pop(priv)

            else:
                raise PrivilegeNotFoundError('User doesn\'t have privilege {0}'.format(name))

        else:
            raise UserNotFoundError('User {0} doesn\'t exist'.format(name))


lm = LM()

def require(*for_privs):
    def decorator(func):
        def wrapper(*args, **kwargs):
            for priv in for_privs:
                if not priv in lm.users[lm._current]:
                    raise PrivilegeError('User {0} doesn\'t have privilege {1}'.format(lm._current, priv))

                return func(*args, **kwargs)

        return wrapper
    return decorator