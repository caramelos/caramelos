# General FS Error
class FSError(Exception):
    pass

# Raised if you try to write to a read-only
# file
class UnsupportedOperation(FSError):
    pass

# File already exists
class FileExistsError(FSError):
    pass

# File not found
class FileNotFoundError(FSError):
    pass

# Is a directory
class IsADirectoryError(FSError):
    pass

# Not a directory
class NotADirectoryError(FSError):
    pass


# Mount error
class MountError(FSError):
    pass

# Mount already exists
class MountExistsError(MountError):
    pass

# Mount not found
class MountNotFoundError(MountError):
    pass