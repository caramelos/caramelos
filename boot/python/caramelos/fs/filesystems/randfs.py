from ..errors import *
from ... import random

class File():
    def __init__(self, parent):
        self.parent = parent

    def write(self):
        raise UnsupportedOperation('Not writeable')

    def writeable(self):
        return False

    def read(self, size = 1):
        if size < 1:
            raise ValueError('Size must be larger than 1')

        return '\n'.join([next(self.parent.rand) for n in range(size)])

    def readable(self):
        return True

    def seek(self, pos, whence = 0):
        raise UnsupportedOperation('Not seekable')

    def seekable(self):
        return False

    def tell(self):
        raise UnsupportedOperation('Can\'t tell')

class RandFS():
    def __init__(self, seed):
        self.rand = random.HashLFSR(seed)

    def mkdir(self, path):
        raise UnsupportedOperation('Directories unsupported')

    def open(self, path):
        return File(self)

    def create(self, path):
        return File(self)

    def exists(self, path):
        raise UnsupportedOperation('Exists unsupported')

    def direxists(self, path):
        raise UnsupportedOperation('Exists unsupported')

    def delete(self, path):
        raise UnsupportedOperation('Delete unsupported')
