from .mnt import root

open = root.open
exists = root.exists
direxists = root.direxists
mkdir = root.mkdir
delete = root.delete