from .fs import get_boot_fs
from .errors import *

class Root():
    def __init__(self):
        self.mounts = {}

    def mounted(self, name):
        '''Returns True if name in self.mounts (mounted)'''
        return name in self.mounts

    def mount(self, name, fs):
        '''Mounts a filesystem under name if name not already mounted
        Raises a MountExistsError if it already exists'''
        if not self.mounted(name):
            self.mounts.update({name: fs})

        else:
            raise MountExistsError('Mount {0} already exists'.format(name))

    def unmount(self, name):
        '''Unmounts a filesystem with the given name
        Raises a MountNotFoundError if it doesn't exist'''
        if self.mounted(name):
            del self.mounts[name]

        else:
            raise MountNotFoundError('Mount {0} not found'.format(name))

    def getmount(self, name):
        '''Returns a mount with the given name
        Raises a MountNotFoundError if it doesn't exist'''
        if self.mounted(name):
            return self.mounts[name]

        else:
            raise MountNotFoundError('Mount {0} not found'.format(name))


    def _getmntname(self, path):
        '''Changes a path into a mount name and a modified path
        Defaults to mount name BootFS'''
        mntname = 'BootFS'

        parts = path.split('/')
        first = parts[0]
        if first[0] == '(' and first[-1] == ')':
            mntname = first[1:-1]
            del parts[0]
            path = '/'.join(parts)
        return mntname, path

    def _run(self, funcname, path, args = None, kwargs = None):
        '''Runs a function with the specified path and args
        Gets mount name from path with the _getmntname function
        Raises a NameError if the function doesn't exist on given mount'''
        mntname, path = self._getmntname(path)
        mnt = self.getmount(mntname)
        if hasattr(mnt, funcname):
            if args:
                if kwargs:
                    return getattr(mnt, funcname)(path, *args, **kwargs)

                else:
                    return getattr(mnt, funcname)(path, *args)

            else:
                if kwargs:
                    return getattr(mnt, funcname)(path, **kwargs)

                else:
                    return getattr(mnt, funcname)(path)

        else:
            raise NameError('Function {0} doesn\'t exist on mount {1}'.format(funcname, mntname))

    def open(self, path, mode):
        return self._run('open', path, [mode])

    def exists(self, path):
        return self._run('exists', path)

    def direxists(self, path):
        return self._run('direxists', path)

    def mkdir(self, path):
        return self._run('mkdir', path)

    def delete(self, path):
        return self._run('delete', path)

root = Root()
root.mount('BootFS', get_boot_fs())