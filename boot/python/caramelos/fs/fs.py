from efi import get_boot_fs as _get_boot_fs
import os
from .errors import *

class Directory():
    def __init__(self, parent, name, raw):
        self.raw = raw
        self.name = name
        self.parent = parent
        self.fns = []
        self.dirs = {}

    def mkdir(self, name):
        if not self.direxists(name):
            ret = Directory(self, name, self.raw.mkdir(name))
            self.dirs.update({name: ret})
            return ret

        else:
            raise IsADirectoryError('Directory {0} Exists'.format(name))

    def open(self, name, mode = 'r'):
        if self.exists(name) and mode == 'r':
            return File(self, name, self.raw.open(name), 'r')

        elif self.exists(name) and mode == 'w':
            return File(self, name, self.raw.open(name), 'w')

        elif not self.exists(name) and mode == 'w':
            ret = File(self, name, self.raw.create(name), 'w')
            self.fns.append(name)
            return ret

        else:
            raise FileNotFoundError('File {0} doesn\'t exist'.format(name))

    def delete(self, name):
        if self.exists(name):
            self.raw.open(name).delete()
            self.fns.pop(name)

        elif self.direxists(name):
            self.dirs[name].delete()
            del self.dirs[name]

        else:
            raise FileNotFoundError('File or directory {0} doesn\'t exist'.format(name))

    def direxists(self, name):
        return name in self.dirs

    def exists(self, name):
        return name in self.fns

class File():
    def __init__(self, parent, name, raw, mode = 'r'):
        self.raw = raw
        self.name = name
        self.parent = parent
        self.mode = mode

    def write(self, s):
        if self.writeable():
            return self.raw.write(s)

        else:
            raise UnsupportedOperation('Not writeable')

    def writeable(self):
        return self.mode == 'w'

    def read(self, size = -1):
        return self.raw.read(size)

    def readable(self):
        return True

    def seek(self, offset, whence = 0):
        return self.raw.seek(offset, whence)

    def seekable(self):
        return True

    def tell(self):
        return self.raw.tell()

    def flush(self):
        return self.raw.flush()

    def close(self):
        return self.raw.close()

    def __del__(self):
        return self.close()

    def __enter__(self):
        return self.raw.__enter__()

    def __exit__(self, exc_type, exc_val, exc_tb):
        return self.raw.__exit__(exc_type, exc_val, exc_tb)

    @property
    def closed(self):
        return self.raw.closed

class FS():
    def __init__(self, raw):
        self.raw = raw
        self.dir = Directory(None, 'root', self.raw)

    def _run(self, funcname, path, args):
        parts, last = os.path.split(path)
        parts = parts.split('/')

        if len(parts) == 0 and path == '/':
            return self.dir

        elif len(parts) == 0 and path != '/':
            return None

        if parts[0] in self.dirs:
            cd = getattr(self.dirs, parts[0])

        else:
            raise FileNotFoundError('Directory {0} doesn\'t exist'.format(parts[0]))

        del parts[0]
        for part in parts:
            if part in cd.dirs:
                cd = getattr(self.dirs, part)

            else:
                raise FileNotFoundError('Directory {0} doesn\'t exist'.format(part))

        if args:
            return getattr(cd, funcname)(last)

        else:
            return getattr(cd, funcname)(last, *args)

    def delete(self, path):
        return self._run('delete', path)

    def mkdir(self, path):
        return self._run('mkdir', path)

    def open(self, path, mode):
        return self._run('open', path, [mode])

    def create(self, path):
        return self.open(path, 'w')

    def exists(self, path):
        return self._run('exists', path)

    def direxists(self, path):
        return self._run('direxists', path)

BootFS = None

def get_boot_fs():
    global BootFS
    if BootFS == None:
        BootFS = FS(_get_boot_fs())
    return BootFS
