from .log import syslog
from time import time
from .fs.mnt import root
from .fs.filesystems.randfs import RandFS
from .random import init as randinit

start = time()

def time_prefix():
    return start - time()

class init_annotation(object):
    def __init__(self, modname):
        self.modname = modname

    # Context management protocol
    def __enter__(self):
        print("{} Init {}".format(time_prefix(), self.modname))
        syslog.info("Init {}".format(self.modname))

    def __exit__(self, exc_type, exc_val, exc_tb):
        print("{} Init {} done".format(time_prefix(), self.modname))
        syslog.info("Init {} done".format(self.modname))

def init():
    with init_annotation('CaramelOS'):
        with init_annotation('random'):
            seed = input('Enter seed for RNG: ')
            randinit(seed)
            root.mount('RandFS', RandFS(seed))
