from .lfsr import lfsr_if, taps_to_poly
from sha256 import sha256

class LFSR():
    def __init__(self, seed):
        self.seed = seed
        self.lfsr = lfsr_if(taps_to_poly((4096, 4095, 4081, 4069)), init=seed)
        self.__next__ = self.next

    def __iter__(self):
        return self

    def next(self):
        return next(self.lfsr)

class HashLFSR(LFSR):
    def next(self):
        return sha256(str(next(self.lfsr)).encode()).hexdigest()

def init(seed):
    global hashlfsr
    hashlfsr = HashLFSR(seed)