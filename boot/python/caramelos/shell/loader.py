from ..fs.funcs import direxists
from ..fs.mnt import root
import sys

class App():
    def __init__(self, mod):
        self.name = mod.name
        self.init = getattr(mod, 'init', lambda s, t: None) # Get the initializer (or an empty function)
        self.main = mod.main

def load():
    if direxists('/bin'):
        fs = root.getmount('BootFS')
        apps = []
        for fn in fs.dir.fns:
            if fn[-3:] == 'py' and fn[0] != '_':
                modname = fn[:-3]
                old = sys.path
                sys.path = ['/bin']
                mod = __import__(modname)
                sys.path = old
                apps.append(App(mod))

            else:
                continue
        return apps

    else:
        return []