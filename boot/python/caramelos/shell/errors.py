class ShellError(Exception):
    pass

class CommandError(ShellError):
    pass

class CommandNotFoundError(CommandError):
    pass

class CommandExistsError(CommandError):
    pass
