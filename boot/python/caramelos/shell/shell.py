from cmd import Cmd
from shlex import split as arg_split
import sys
from ..fs.funcs import exists, direxists
from ..lm import lm
from .errors import *
from .loader import load as load_apps

class RawTerminal(Cmd):
    def add_command(self, name, func):
        func_name = 'do_' + name

        if not hasattr(self, func_name):
            def wrapped(self, line):
                '''Function that splits the line into arguments and passes them to the function'''
                args = arg_split(line[0])
                return func(args)

            setattr(self, func_name, wrapped)

        else:
            raise CommandExistsError('Command {0} already exists'.format(name))

    def delete_command(self, name):
        func_name = 'do_' + name

        if hasattr(self, func_name):
            delattr(self, func_name)

        else:
            raise CommandNotFoundError('Command {0} doesn\'t exist'.format(name))

    def rename_command(self, old_name, new_name):
        old_func_name = 'do_' + old_name
        new_func_name = 'do_' + new_name

        if hasattr(self, old_func_name):
            if not hasattr(self, new_func_name):
                setattr(self, new_func_name, getattr(self, old_func_name))
                delattr(self, old_func_name)

            else:
                raise CommandExistsError('Command {0} already exists'.format(new_name))

        else:
            raise CommandNotFoundError('Command {0} doesn\'t exist'.format(old_name))

    # Built in commands
    def do_exit(self, args):
        '''Exits the shell'''
        return True # Returning True exits the shell

    def do_quit(self, args):
        '''Quits the shell - same as exit'''
        return self.do_exit(args) # Just runs do_exit

    def do_whoami(self, args):
        '''Print the current user name'''
        print(lm._current)

    def do_login(self, args):
        '''Changes your user'''
        if len(args) != 1:
            print('Usage: login <username>')
            return
        lm.login(args[0])
        self.prompt = '{0} @ .> '.format(lm._current)

class Terminal():
    def __init__(self, stdin = sys.stdin, stdout = sys.stdout):
        self.term = RawTerminal(stdin, stdout)
        self.term.prompt = '{0} @ .> '.format(lm._current)

        self.stdin = stdin
        self.stdout = stdout

        self.perm = ['exit', 'quit', 'help', 'shell']

        self.intro = '''Welcome to the . shell
        Type ? or help into the shell for help
        Enjoy!'''

        self.cwd = '/'

    def getcwd(self):
        return self.cwd

    def chdir(self, path):
        if direxists(path):
            if not exists(path):
                self.cwd = path

            else:
                raise NotADirectoryError('Path {0} is a file'.format(path))

        else:
            raise FileNotFoundError('Path {0} doesn\'t exist'.format(path))

    # Adds, removes and renames terminal commands
    def add_command(self, name, func):
        if name in self.perm:
            raise CommandError('Command {0} is permanent'.format(name))
        return self.term.add_command(name, func)

    def delete_command(self, name):
        if name in self.perm:
            raise CommandError('Command {0} is permanent'.format(name))
        return self.term.delete_command(name)

    def rename_command(self, old_name, new_name):
        if old_name in self.perm:
            raise CommandError('Command {0} is permanent'.format(old_name))
        return self.term.rename_command(old_name, new_name)

    def run(self):
        apps = load_apps()
        for app in apps:
            app.init(self)
            self.add_command(app.name, app.main)

        self.term.cmdloop(self.intro)

shell = Terminal()