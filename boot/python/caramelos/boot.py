from log import syslog
from .init import init
from .shell.shell import shell
import sys

def boot():
    syslog.info('Booting CaramelOS')
    print('Booting CaramelOS')
    init()
    print('')
    print('Loading Shell')
    shell.run()
    print('Shell exited - Bye!')
    sys.exit()