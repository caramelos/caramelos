class ReadOnlyPropertyError(Exception):
    pass

def rop(obj, name):
    def getter(self):
        return obj

    def setter(self, value):
        raise ReadOnlyPropertyError('Can\'t modify property {0}'.format(name))

    prop = property(getter, setter)
    return prop