'''pow(x, y) = x^y
root(x, root) = x ** (1 / root) so that root(x, 2) = x ** 0.5
sqrt and cbrt calculate root(x, 2) and root(x, 3) respectively'''
def pow(x, y):
    return x ** y

def root(x, root):
    y = 1 / root
    return x ** y

def sqrt(x):
    return root(x, 2)

def cbrt(x):
    return root(x, 3)
