# CaramelOS
---
## About
CaramelOS is a python OS written on top of the BIOS Implementation Test Suite [here](https://biosbits.org/download) and on github [here](https://github.com/biosbits/bits]). To get a copy of bits source run `git clone --recursive https://github.com/biosbits/bits` (the `--recursive` flag is to clone all submodules too, the other repositories on [github](https://github.com/biosbits]). The .zip on the biosbits.org website has a boot and an efi directory. Under /boot/src/ there is a file containing the source code. See /INSTALL.txt for instructions.

## Downloading
To download CaramelOS, clone it from bitbucket with `git clone https://bitbucket.com/rybot666/caramelos.git`.